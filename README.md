# [![Egeriactl](https://gitlab.com/egeria/egeria-ctl/raw/master/logo/logo_small.png)](https://gitlab.com/egeria/egeria-ctl) Egeriactl

# Egeria bestows wisdom and knowledge.

![Egeria](http://i.imgur.com/cCUfMh3.jpg)<br />
*Egeria, the SFW nymph, counseling Numa Pompilius*

`egeria-ctl` lets you control a running Egeria instance. Like Egeria itself, this is pre-pre-pre-pre-alpha software and you shouldn't use it.

