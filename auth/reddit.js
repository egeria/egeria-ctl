var Promise = require('bluebird');

module.exports = {
    sanity: {
        name: {type: String, mandatory: true},
        clientId: {type: String, mandatory: true},
        clientSecret: {type: String, mandatory: true},
        username: {type: String, mandatory: true},
        password: {type: String, mandatory: true}
    },
    docs: {
        name: 'The name you want to use to reference this identity in Egeria\'s configuration file',
        clientId: 'Get it from reddit',
        clientSecret: 'Get it from reddit',
        username: 'Your username',
        password: 'Your password'
    },
    authorize(options) {
        return Promise.resolve({
            type: 'reddit',
            name: options.name,
            clientId: options.clientId,
            clientSecret: options.clientSecret,
            username: options.username,
            password: options.password
        });
    }
};
