var Promise = require('bluebird');

var auth = (options) => {
    return Promise.resolve({
        type: 'pushbullet',
        name: options.name,
        key: options.key
    });
};

module.exports = {
    sanity: {
        name: {type: String, mandatory: true},
        key: {type: String, mandatory: true}
    },
    docs: {
        name: 'The name you want to use to reference this identity in Egeria\'s configuration file',
        key: 'Your API key. Get it from your profile page on pushbullet.com'
    },
    authorize(options) {
        return auth(options);
    }
};
