var R = require('ramda'),
    Promise = require('bluebird'),
    tools = require('egeria-tools'),
    http = require('http'),
    urlparse = require('url').parse,
    open = require('open'),
    _request = require('request');

var request = _request.defaults({
    jar: true,
    headers: {'User-Agent': 'Egeria/egeria-ctl/alpha'}
});

var shutdownServer;

var auth = (options) => {
    var solve, ject;

    var out = new Promise ((resolve, reject) => {
        solve = resolve;
        ject = reject;
    });

    var handleRequest = (req, res) => {
        var urldata = R.defaultTo({}, urlparse(req.url, true));

        if (urldata.pathname == '/auth') {
            var data = R.defaultTo({}, urldata.query);
            if (!R.isNil(data.error)) {
                res.end('Authorization failed.');
                shutdownServer();
                ject(data.error);
            } else if (!R.isNil(data.code)) {
                res.end('Authorization received, you can close this tab/window now. We\'re done here.');
                shutdownServer();
                request({
                    uri: 'https://www.googleapis.com/oauth2/v4/token',
                    method: 'post',
                    form: {
                        code: data.code,
                        client_id: options.clientId,
                        client_secret: options.clientSecret,
                        redirect_uri: 'http://localhost:54322/auth',
                        grant_type: 'authorization_code'
                    }
                }, (err, res, body) => {
                    if (err) {
                        ject(err);
                    } else {
                        var data;
                        try {
                            data = JSON.parse(body);
                        } catch(e) {
                            data ={error: 'parse_error', error_description: 'Bad response'};
                        }
                        if (!R.isNil(data.error)) {
                            ject(data.error + ' - ' + data.error_description);
                        } else {
                            solve({
                                type: 'google',
                                name: options.name,
                                clientId: options.clientId,
                                clientSecret: options.clientSecret,
                                refreshToken: data.refresh_token
                            });
                        }
                    }
                });
            } else {
                res.end('Authorization failed.');
                tools.trace(urldata);
                shutdownServer();
                ject('Unknown error.');
            }
        } else {
            res.writeHead(302, {
                'Location': 'https://accounts.google.com/o/oauth2/v2/auth?' +
                    'scope=https://www.googleapis.com/auth/drive https://www.googleapis.com/auth/youtube.readonly https://www.googleapis.com/auth/gmail.insert https://www.googleapis.com/auth/calendar' +
                    '&redirect_uri=http://localhost:54322/auth' +
                    '&response_type=code' +
                    '&client_id=' + options.clientId
            });
            res.end();
        }
    };

    var server = http.createServer(handleRequest);

    var sockets = {}, socketId = 0;
    server.on('connection', (socket) => {
        var id = socketId++;
        sockets[id] = socket;
        socket.on('close', () => {
            delete sockets[socketId];
        });
    });

    server.listen(54322, () => {
        console.log('Handing control over to your default browser...');
        open('http://localhost:54322');
    });


    shutdownServer = () => {
        server.close();
        for (var socketId in sockets) {
            sockets[socketId].destroy();
        }
        console.log('You can now close your browser if you want.');
    };

    return out;
};

module.exports = {
    sanity: {
        name: {type: String, mandatory: true},
        clientId: {type: String, mandatory: true},
        clientSecret: {type: String, mandatory: true}
    },
    docs: {
        name: 'The name you want to use to reference this identity in Egeria\'s configuration file',
        clientId: 'Your client id as displayed in the Google developer console',
        clientSecret: 'Your client secret as displayed in the Google developer console'
    },
    authorize(options) {
        return auth(options);
    }
};
