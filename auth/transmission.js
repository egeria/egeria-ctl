var Promise = require('bluebird');

var auth = (options) => {
    return Promise.resolve({
        type: 'transmission',
        name: options.name,
        username: options.username,
        password: options.password
    });
};

module.exports = {
    sanity: {
        name: {type: String, mandatory: true},
        username: {type: String, mandatory: true},
        password: {type: String, mandatory: true}
    },
    docs: {
        name: 'The name you want to use to reference this identity in Egeria\'s configuration file',
        username: 'Your Transmission RPC username',
        password: 'Your Transmission RPC password'
    },
    authorize(options) {
        return auth(options);
    }
};
