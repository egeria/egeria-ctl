var R = require('ramda'),
    Promise = require('bluebird');

module.exports = {
    sanity: {
        name: {type: String, mandatory: true},
        username: {type: String, mandatory: true},
        password: {type: String, mandatory: true},
        address: {type: String, mandatory: true},
        service: {type: String, mandatory: false},
        host: {type: String},
        port: {type: String},
        secure: {type: String}
    },
    docs: {
        name: 'The name you want to use to reference this identity in Egeria\'s configuration file',
        username: ' ',
        password: ' ',
        address: ' ',
        service: ' ',
        host: ' ',
        port: ' ',
        secure: ' '
    },
    authorize(options) {
        if (options.service !== 'none') {
            return Promise.resolve({
                type: 'email',
                name: options.name,
                username: options.username,
                password: options.password,
                address: options.address,
                service: options.service
            });
        } else if (!R.isNil(options.host) && !R.isNil(options.port)) {
            return Promise.resolve({
                type: 'email',
                name: options.name,
                username: options.username,
                password: options.password,
                address: options.address,
                host: options.host,
                port: options.port,
                secure: (options.secure == '1' ? true : false)
            });
        } else {
            return Promise.reject('Either specify a valid service or specify host and port');
        }
    }
};

