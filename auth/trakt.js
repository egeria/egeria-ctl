var R = require('ramda'),
    tools = require('egeria-tools'),
    Contraktor = require('contraktor'),
    open = require('open');


var auth = (options) => {
    var trakt = new Contraktor({
        endpoint: 'production',
        headers: {'User-Agent': 'Node.js/Egeria/Contraktor/alpha'},
        identity: {
            client_id: options.client_id,
            client_secret: options.client_secret
        }
    });

    var warn = (data) => {
        open(data.verification_url);
        console.log('Go to ' + data.verification_url + ' (a browser window or a new tab should have been opened for you just now)');
        console.log(' ');
        console.log('Input the code ' + data.user_code + ' to authorize Egeria.');
        return data;
    };

    var step2 = R.bind(trakt.device_auth_step2, trakt);

    var auth = trakt
        .device_auth_step1()
        .then(warn)
        .then(step2)
        .then(R.pipe(
            R.assoc('name', options.name),
            R.assoc('redirect_uri', 'urn:ietf:wg:oauth:2.0:oob')
        ))
        .catch((e) => {
            console.log('Authorization FAILED');
            tools.trace(e);
        });

    return auth;
};

module.exports = {
    sanity: {
        name: {type: String, mandatory: true},
        client_id: {type: String, mandatory: true},
        client_secret: {type: String, mandatory: true}
    },
    docs: {
        name: 'The name you want to use to reference this identity in Egeria\'s configuration file',
        client_id: 'Your client id. To get it create a new app in your Trakt\'s profile page',
        client_secret: 'Your client secret. It\'s in the app details once you have created it.'
    },
    authorize(options) {
        return auth(options);
    }
};
