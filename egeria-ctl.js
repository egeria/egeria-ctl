#! /usr/bin/env node

var R = require('ramda'),
    redis = require('redis'),
    program = require('commander'),
    froid = require('froid'),
    Promise = require('bluebird');

Promise.promisifyAll(redis.RedisClient.prototype);
Promise.promisifyAll(redis.Multi.prototype);

var isMissing = R.either(R.isNil, R.isEmpty);

var fail = (message) => {
    console.log('FATAL: ' + message);
    process.exit(1);
};

var success = (message) => {
    console.log('Operation succeeded. ' + message);
    process.exit(0);
};

var client;

var store = (data) => {
    if (isMissing(data.name)) {
        return Promise.reject('Identity name missing. Data: ' + JSON.stringify(data));
    }
    var key = 'egeria:auth:' + data.name;
    return client.SETAsync(key, JSON.stringify(data))
        .then(() => client.quitAsync())
        .then(() => success('Identity stored correctly.'));
};

var auth = (config, type, args) => {
    var lib, options = {};
    client = redis.createClient(config.port, config.host);
    client.on('error', (e) => {
        console.log('FATAL ERROR: ' + e);
        process.exit(1);
    });

    try {
        lib = require('./auth/' + type + '.js');
    } catch(e) {
        fail('Identity ' + type + ' nonexistant or unsupported.');
    }
    R.forEach((p) => {
        options[p] = args.shift();
    }, R.keys(lib.sanity));

    var err = froid(lib.sanity, options);

    if (!isMissing(err)) {
        console.log('Please provide all of the information needed to create the chosen identity in the specified order:');
        console.log();
        R.mapObjIndexed((d, o) => console.log(o + ': ' + d), lib.docs);
        console.log();
        fail(err);
    }

    return lib.authorize(options).then(store).catch(fail);
};

program.version(require('./package.json').version);

program
    .option('-h --host [host]', 'Redis host, default is localhost')
    .option('-p --port [port]', 'Redis port, default is 6379');

program
    .command('auth <type> [credentials...]')
    .action((type, credentials) => {
        auth({
            host: R.defaultTo('localhost', program.host),
            port: R.defaultTo(6379, program.port)
        }, type, credentials);
    });

program.parse(process.argv);


